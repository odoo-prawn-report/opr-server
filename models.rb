# coding: utf-8

require 'active_record'
require 'yaml'

config = YAML.load_file('config/db.yaml')

ActiveRecord::Base.establish_connection(config)

class ResCompany < ActiveRecord::Base
	self.table_name = "res_company"
	has_one :respartner, foreign_key: "partner_id"
end

class ResPartner < ActiveRecord::Base
	self.table_name = "res_partner"
	self.inheritance_column = :_type_disabled
end

class ResPartnerBank < ActiveRecord::Base
	self.table_name = "res_partner_bank"
end

class ResBank < ActiveRecord::Base
	self.table_name = "res_bank"
end

class AccountTax < ActiveRecord::Base
	self.table_name = "account_tax"
	self.inheritance_column = :_type_disabled
end

class SaleOrder < ActiveRecord::Base
	self.table_name = "sale_order"
	belongs_to :respartner, foreign_key: "partner_id"
	has_many :saleorderline
end

class SaleOrderLine < ActiveRecord::Base
	self.table_name = "sale_order_line"
	belongs_to :saleorder, foreign_key: "order_id"
	belongs_to :productproduct, foreign_key: "product_id"
	has_many :saleordertax
end

class SaleOrderTax < ActiveRecord::Base
	self.table_name = "sale_order_tax"
	belongs_to :account_tax
end

class AccountInvoice < ActiveRecord::Base
	self.table_name = "account_invoice"
	self.inheritance_column = :_type_disabled
end

class AccountInvoiceLine < ActiveRecord::Base
	self.table_name = "account_invoice_line"
end

class AccountInvoiceLineTax < ActiveRecord::Base
	self.table_name = "account_invoice_line_tax"
end

class AccountVoucher < ActiveRecord::Base
	self.table_name = "account_voucher"
	self.inheritance_column = :_type_disabled
end

class AccountVoucherLine < ActiveRecord::Base
	self.table_name = "account_voucher_line"
	self.inheritance_column = :_type_disabled
end

class AccountMoveLine < ActiveRecord::Base
	self.table_name = "account_move_line"
	self.inheritance_column = :_type_disabled
end

class AccountMove < ActiveRecord::Base
	self.table_name = "account_move"
	self.inheritance_column = :_type_disabled
end

class AccountJournal < ActiveRecord::Base
	self.table_name = "account_journal"
	self.inheritance_column = :_type_disabled
end

class ProductProduct < ActiveRecord::Base
	self.table_name = "product_product"
	has_many :saleorderline
end

class AccountPaymentTerm < ActiveRecord::Base
	self.table_name = "account_payment_term"
	self.inheritance_column = :_type_disabled
end

class StockPicking < ActiveRecord::Base
	self.table_name = "stock_picking"
	self.inheritance_column = :_type_disabled
end

class StockPickingType < ActiveRecord::Base
	self.table_name = "stock_picking_type"
	self.inheritance_column = :_type_disabled
end

class StockMove < ActiveRecord::Base
	self.table_name = "stock_move"
	self.inheritance_column = :_type_disabled
end

class SavIntervention < ActiveRecord::Base
	self.table_name = "sav_intervention"
end

class SavInterventionLine < ActiveRecord::Base
	self.table_name = "sav_intervention_line"
end

class SavTechnicien < ActiveRecord::Base
	self.table_name = "sav_technicien"
end

class SavAppareil < ActiveRecord::Base
	self.table_name = "sav_appareil"
end

class ResUser < ActiveRecord::Base
	self.table_name = "res_users"
end

class PurchaseOrder < ActiveRecord::Base
	self.table_name = "purchase_order"
end

class PurchaseOrderLine < ActiveRecord::Base
	self.table_name = "purchase_order_line"
end

class PurchaseOrderTax < ActiveRecord::Base
	self.table_name = "purchase_order_taxe"
end

class ResCurrency < ActiveRecord::Base
	self.table_name = "res_currency"
end

class StockPackOperation < ActiveRecord::Base
	self.table_name = "stock_pack_operation"
end

class StockProductionLot < ActiveRecord::Base
	self.table_name = "stock_production_lot"
end

class ProductUom < ActiveRecord::Base
	self.table_name = "product_uom"
end

class IrTranslation < ActiveRecord::Base
	self.table_name = "ir_translation"
	self.inheritance_column = :_type_disabled
end

class AccountAnalyticAccount < ActiveRecord::Base
  self.table_name = "account_analytic_account"
	self.inheritance_column = :_type_disabled
end

class SavSymptome < ActiveRecord::Base
	self.table_name = "sav_symptome"
end

class SavOutil < ActiveRecord::Base
	self.table_name = "sav_outil"
end

class SavOs < ActiveRecord::Base
	self.table_name = "sav_os"
end

class SavType < ActiveRecord::Base
	self.table_name = "sav_type"
end

class SavInterventionTechnicien < ActiveRecord::Base
	self.table_name = "sav_intervention_technicien"
end

class DecimalPrecision < ActiveRecord::Base
	self.table_name = "decimal_precision"
end
