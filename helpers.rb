# encoding: UTF-8

def edition type, id, options=nil
	
	ids = id.split("-").map { |s| s.to_i }
	fichier = File.join(settings.public_folder, "#{type}-#{ids[0,5].join("-")}-#{DateTime.now.strftime('%Y%m%d%H%M%S')}.pdf")
	case type
		when "commande" then EditionCommande.new ids, fichier, @@options
		when "facture" then EditionFacture.new ids, fichier, @@options
		when "bl" then EditionBl.new ids, fichier, @@options
		when "bi" then EditionBi.new ids, fichier, @@options
		when "commandefournisseur" then EditionCommandeFournisseur.new ids, fichier, @@options
		when "demandeprix" then EditionDemandePrixFournisseur.new ids, fichier, @@options
	end
	
	Thread.new{
		sleep 15
		File.delete(fichier) if File.exist?(fichier)
	}
	
	send_file fichier if File.exist?(fichier)
	
end

def get_options
	options = YAML.load_file('config/options.yaml')
	DecimalPrecision.all.each do |precision|
		options.merge!({precision.name => precision.digits})
	end
	p options
	return options
end
