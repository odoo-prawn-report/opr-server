#!/usr/bin/env ruby
# coding: utf-8

##############################################################################
#		 odoo_prawn_report: Report Prawn Server for Odoo
#
#    Copyright (c) 2014 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re> 
#
# 	 This file is part of odoo_prawn_report.
#
#    odoo_prawn_report is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    odoo_prawn_report is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

require "bundler/setup"
require "sinatra"
require "sinatra/reloader" if development?
require 'prawn'
require 'prawn/table'
require 'stringio'
require './models.rb'
require './helpers.rb'

@@options = get_options
template_folder = 'templates/' + @@options[:template]
@@template_folder = template_folder
require "./" + template_folder + "/edition.rb" if File.exist?("./" + template_folder + "/edition.rb")
Dir.foreach(template_folder) { |f|
	require "./" + template_folder + "/#{f}" if (File.extname(f).eql?(".rb") and f.include? "edition_")
}

# Devis et bons de commande
get '/sale.report_saleorder/:id' do
	edition "commande", params[:id], @@options
end

# Factures d'achat, de vente et avoirs
get '/account.report_invoice/:id' do
	edition "facture", params[:id], @@options
end

# Bons de livraison
get '/stock.report_picking/:id' do
	edition "bl", params[:id], @@options
end

# Bons d'intervention
get '/sav.intervention/:id' do
	edition "bi", params[:id], @@options
end

# Commande fournisseur
get '/purchase.report_purchaseorder/:id' do
	edition "commandefournisseur", params[:id], @@options
end

# Demande de prix
get '/purchase.report_purchasequotation/:id' do
	edition "demandeprix", params[:id], @@options
end
