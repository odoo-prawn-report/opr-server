# coding: utf-8

##############################################################################
#		 edition: Report for odoo_prawn_report
#
#    Copyright (c) 2015 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re> 
#
# 	 This file is part of odoo_prawn_report.
#
#    odoo_prawn_report is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    odoo_prawn_report is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

class Edition < Prawn::Document
	
	attr :type_document
	
	def initialize ids, fichier, options
	
		super(	
					:page_layout => :portrait,
					:right_margin => 15,
					:page_size => 'A4'
			 	 )
			 	 
		@societe = ResCompany.first 
		@partner_society = ResPartner.find(@societe.partner_id)
		
		@options = options
		
		font_families.update("Aclonica" => {
			:normal => @@template_folder + "/fonts/Aclonica.ttf"
		})
		
		font_families.update("Cantarell" => {
			:normal => @@template_folder + "/fonts/Cantarell-Regular.ttf",
			:italic => @@template_folder + "/fonts/Cantarell-Oblique.ttf",
			:bold => @@template_folder + "/fonts/Cantarell-Bold.ttf",
			:bold_italic => @@template_folder + "/fonts/Cantarell-BlodOblique.ttf"
		})
		
		font "Cantarell"
		
		@red = "720000"
		@grey = "eeeeee"
		
	end
	
	def en_tete opts
		
		pas = 10
		text_size = 10
		top = bounds.top-10
		
		top -= pas+35
		transitaire = opts[:transitaire]
		livraison = opts[:livraison]
		
		rect3 = (!transitaire.nil? or !livraison.nil?)
		
		draw_text "Emetteur :", :at => [0,top], :size => 8, :styles => [:bold]
		draw_text "Transitaire :", :at => [180,top], :size => 8, :styles => [:bold] if transitaire
		draw_text "Livraison à :", :at => [180,top], :size => 8, :styles => [:bold] if livraison
		draw_text "Adressé à :", :at => [(rect3 ? 360 : 250),top], :size => 8, :styles => [:bold]
		
		top -= pas
		
		fill_color @grey
		stroke_color @red
		rectangle [0,top+5], (rect3 ? 170 : 230), 120
		fill
		if rect3
			rectangle [180,top+5], 170, 120
			stroke
		end
		rectangle [(rect3 ? 360 : 250),top+5], (rect3 ? 180 : 290), 120
		stroke
		
		fill_color "000000"
		
		text_size = 10
		
		partner = ResPartner.find(opts[:partner_id])
		partner_facturation = (partner.parent_id.to_i>0 ? ResPartner.find(partner.parent_id) : partner)
		
		bounding_box [bounds.left, bounds.top+20], :width  => bounds.width do
			unless @partner_society.image.to_s.empty?
				image StringIO.new(Base64.decode64(@partner_society.image)), :height => 70 
			else
				move_down 70
			end
		end
		formatted_text_box([{ :text => "#{@type_document.upcase}\n",
													:size => 	text_size+4,
													:font => "Aclonica",
													:color => @red
												},
												{ :text => "Réf. : #{opts[:reference]}\n",
													:size => 	text_size+4,
													:font => "Aclonica",
													:color => @red
												},
												{ :text => (opts[:origin] ? "Document d'origine : #{opts[:origin]}\n" : ""),
													:size => text_size
												},
												{ :text => (opts[:partner_ref] ? "Réf. partenaire : #{opts[:partner_ref]}\n" : ""),
													:size => 	text_size,
												},
												{ :text => "Date : #{opts[:date]}\n",
												  :size => text_size
												}],
												{ :at => [350,bounds.top+10],
												  :width => bounds.right-350,
												  :align => :right }
												)	
				          	
		formatted_text_box([{ :text => @societe.name+"\n",
													:size => 	text_size+1,
													:styles => [:bold]
												},
								        { :text => "#{@partner_society.street}\n#{@partner_society.street2}\n#{@partner_society.zip}\n#{@partner_society.city}\n\n",
								          :size => text_size,
								        },
								        { :text => "Téléphone: #{@partner_society.phone}\nFax: #{@partner_society.fax}\n",
								          :size => text_size,
								        },
								        { :text => "Email: #{@partner_society.email}\n",
								          :size => text_size,
								        },
								        { :text => "Web: #{@partner_society.website}\n",
								          :size => text_size,
								        }],
								        {:at => [7,top],
								        :width => 156}
								        )
		if transitaire
			formatted_text_box([{ :text => "#{transitaire.name}\n\n",
														:size => 	text_size+1,
														:styles => [:bold]
													},
										      { :text => "#{transitaire.street}\n#{transitaire.street2}\n",
										        :size => text_size,
										      },
										      { :text => "#{transitaire.zip} #{transitaire.city}\n",
										        :size => text_size,
										      },
										      { :text => (transitaire.phone ? "Tél: #{transitaire.phone}\n" : ""),
										        :size => text_size,
										      },
										      { :text => (transitaire.fax ? "Fax: #{transitaire.fax}\n" : ""),
										        :size => text_size,
										      }],
										      {:at => [187,top],
										      :width => 156}
										      )
		end
		if livraison
			formatted_text_box([{ :text => "#{livraison.name}\n\n",
														:size => 	text_size+1,
														:styles => [:bold]
													},
										      { :text => "#{livraison.street}\n#{livraison.street2}\n",
										        :size => text_size,
										      },
										      { :text => "#{livraison.zip} #{livraison.city}\n",
										        :size => text_size,
										      },
										      { :text => (livraison.phone ? "Tél: #{livraison.phone}\n" : ""),
										        :size => text_size,
										      },
										      { :text => (livraison.fax ? "Fax: #{livraison.fax}\n" : ""),
										        :size => text_size,
										      }],
										      {:at => [187,top],
										      :width => 156}
										      )
		end
		formatted_text_box([{ :text => "#{partner_facturation.name}\n\n",
							  :size => 	text_size+1,
							  :styles => [:bold]
							},
				            { :text => "#{partner_facturation.street}\n#{partner_facturation.street2}\n",
				              :size => text_size,
				            },
				            { :text => "#{partner_facturation.zip} #{partner_facturation.city}\n",
				              :size => text_size,
				            },
				            { :text => (partner_facturation.phone ? "Tél: #{partner_facturation.phone}\n" : ""),
				              :size => text_size,
				            },
				            { :text => (partner_facturation.fax ? "Fax: #{partner_facturation.fax}\n" : ""),
				              :size => text_size,
				            },
				            { :text => (partner_facturation.email ? "Email: #{partner_facturation.email}\n" : ""),
				              :size => text_size,
				            }],
				            {:at => [(rect3 ? 367 : 257),top],
				            :width => 156}
				            )
		top = top-120
		
		unless opts[:notes].to_s.empty? then
			bounding_box([0, top], :width => 540) do
				font_size(text_size-4) do
					text opts[:notes], :size => text_size
				end
			end
		end
	
	end
	
	def corps note=false, data, taille_colonnes
		
		i = note ? 1 : 10
		text "\n" * i
		
		t = table(data) do |table|
			table.header = true
			table.row(0).style :borders => [:top, :bottom, :left, :right], :background_color => @red, :text_color => "ffffff", :font => "Aclonica"
			table.cell_style = { :size => 8, :border_color => @red}
			table.row(1..data.count).style :borders => [:left, :right], :border_color => @grey
			table.column(1..5).align = :right
			table.row(0).style :align => :center
			table.column(0).align = :left
			table.column_widths = taille_colonnes
		end 
		
		top = cursor
		
		stroke_color @grey
		
		left = bounds.left
		bottom = 200
		if top<bottom then
			bottom = 14
		end
		line [left,top], [left,bottom]
		taille_colonnes.each { |i|
			left += i
			line [left,top], [left,bottom]
		}
		line [bounds.left,bottom], [left,bottom]
		stroke
  		
		if bottom<200 then
			start_new_page
		end
		
	end
	
	def total tva_tab, reste=false

		top = 200
		text_size = 8
		
		data = []
		
		data << ["Total HT", "%.2f €" % @document.amount_untaxed]
		
		tva_tab.each do |key, value|
			data << [value[:name], "%.2f €" % value[:total]] if value[:total] > 0
		end
		
		data << ["Total TTC", "%.2f €" % @document.amount_total]
		
		if reste
			if (@document.residual<@document.amount_total and !@document.number.to_s.empty?	and @journal.type != 'sale_refund')
				data << ["Payé", "%.2f €" % (@document.amount_total-@document.residual)]
				data << ["Reste à payer", "%.2f €" % @document.residual]
			end
		end
		
		bounding_box([310,top], :width => 300, :height => 200) do
			table(data, :cell_style => { :borders => [], :padding => 1, :size => text_size+2 }) do |table|
	  			table.column(0).align = :left
	  			table.column(1).align = :right
	  			table.row(data.count-1).style :background_color => @red, :text_color => "ffffff", :font => "Aclonica"
	  			table.column(0).width = 150
	  			table.column(1).width = 80
			end 
		end
	end
	
	def quantite_totale qte

		top = 200
		text_size = 8
		
		data = []
		
		data << ["Quantité totale", "%.2f" % qte]
		
		bounding_box([390,top-5], :width => 150, :height => 200) do
			
			table(data, :cell_style => { :borders => [], :padding => 1, :size => text_size+2 }) do |table|
	  			table.column(0).align = :left
	  			table.column(1).align = :right
	  			table.row(data.count-1).style :background_color => @red, :text_color => "ffffff", :font => "Aclonica"
	  			table.column(0).width = 100
	  			table.column(1).width = 50
			end 
		end
		
	end
		
	def conditions
		# Condition et mode de règlement
		top = 200
		text_size = 8
		compte = ResPartnerBank.where(:partner_id => @partner_society.id).first
		banque = ResBank.find(compte.bank)
		condition = ""
		begin
			if @document.payment_term
				payment_term = AccountPaymentTerm.find(@document.payment_term)
				condition = payment_term.note 
			end
		rescue
		end
		loi = @options[:loi]
		formatted_text_box([{ :text => "Condition de règlement: #{condition}\n\n",
							  :size => 	text_size,
							  :styles => [:bold]
							},
				            { :text => "Règlement par chèque à l'ordre de #{@societe.name}\n",
				              :size => text_size,
				              :styles => [:bold]
				            },
				            { :text => "envoyé au:\n",
				              :size => text_size,
				              :styles => [:bold]
				            },
				            { :text => "#{@partner_society.street}\n#{@partner_society.street2}\n#{@partner_society.zip} #{@partner_society.city}\n\n",
				              :size => text_size
				            },
				            { :text => "Règlement par virement sur le compte bancaire suivant:\n",
				              :size => text_size,
				              :styles => [:bold]
				            },
				            { :text => (compte ? "Banque: #{banque.name}\n" : ""),
				              :size => text_size
				            },
				            { :text => (compte ? "BIC: #{compte.bank_bic}\n" : ""),
				              :size => text_size
				            },
				            { :text => (compte ? "IBAN: #{compte.acc_number}\n\n" : ""),
				              :size => text_size
				            },
				            { :text => loi,
				              :size => text_size-2
				            }],
				            {:at => [0,top-5]}
				            )
		
	end
	
	def liste_paiements
		# Liste des paiements déjà effectués
		top = 200
		text_size = 8
		moveline = AccountMoveLine.where("move_id = #{@document.move_id} and reconcile_id > 0").first if @document.move_id
		if moveline	
			paiements = AccountMoveLine.where(:reconcile_id => moveline.reconcile_id)
			if paiements.count>1
				data = [["Date", "Mode", "Référence", "Montant"]]
				paiements.each { |p|
					if p.move_id!=@document.move_id
						journal = AccountJournal.find(p.journal_id)
						if @journal.type == 'purchase'
							data << [p.date.strftime("%d/%m/%Y"), journal.name, p.ref, "%.2f €" % p.debit]
						else
							data << [p.date.strftime("%d/%m/%Y"), journal.name, p.ref, "%.2f €" % p.credit]
						end
					end
				}

				bounding_box([310,top-90], :width => 300) do	
					text "Règlements déjà effectués :", :size => text_size, :style => :bold
					table(data, :cell_style => { :borders => [], :padding => 1, :size => text_size-2 }) do |table|
						table.row(0).font_style = :bold
						table.column(0).width = 50
						table.column(1).width = 50
						table.column(2).width = 60
						table.column(3).width = 60
					end
				end
			end
		end
	end
	
	def rendu fichier
	
		number_pages "#{@societe.rml_header1} - SIRET: #{@societe.siret}", {:start_count_at => 1, :at => [0, 10], :align => :center, :size => 8}
		number_pages "APE: #{@societe.ape} - RCS/RM: #{@societe.company_registry}", {:start_count_at => 1, :at => [0, 0], :align => :center, :size => 8}
		render_file fichier
	
	end
	
end
