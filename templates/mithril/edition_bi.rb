# coding: utf-8

##############################################################################
#		 edition_commande: Report Sale Order for odoo_prawn_report
#
#    Copyright (c) 2014 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re> 
#
# 	 This file is part of odoo_prawn_report.
#
#    odoo_prawn_report is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    odoo_prawn_report is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

class EditionBi < Prawn::Document
	
	def initialize ids, fichier, options
	
		super(	
					:page_layout => :portrait,
					:right_margin => 15,
					:page_size => 'A4'
			 	 )
			 	 
		@societe = ResCompany.first 
		@partner_society = ResPartner.find(@societe.partner_id)
		
		font_families.update("Aclonica" => {
			:normal => @@template_folder + "/fonts/Aclonica.ttf"
		})
		
		font_families.update("Cantarell" => {
			:normal => @@template_folder + "/fonts/Cantarell-Regular.ttf",
			:italic => @@template_folder + "/fonts/Cantarell-Oblique.ttf",
			:bold => @@template_folder + "/fonts/Cantarell-Bold.ttf",
			:bold_italic => @@template_folder + "/fonts/Cantarell-BlodOblique.ttf"
		})
		
		font "Cantarell"
		
		@red = "720000"
		@grey = "eeeeee"
		@grey_dark = "aaaaaa"
		
		i=0
		ids.each { |id|
			preparation id
			i +=1
			start_new_page unless i.eql?(ids.count)
		}
		
		pieds 
		rendu fichier
	end
	
	def preparation id
		@document = SavIntervention.find(id)
		@documents_lignes = SavInterventionLine.where(:intervention_id => id).order(:id)
	
		page_garde
		corps
	end
	
	def page_garde
		
		pas = 10
		text_size = 10
		top = bounds.top-10
		
		top -= pas+35
		
		top -= pas
		
		fill_color @red
		rectangle [bounds.right-80,bounds.top+80], 100, bounds.top+160
		fill
		fill_color "000000"
		
		text_size = 10
		
		type_document = case @document.state
			when "demande" then "demande d'intervention"
			when "bon" then "bon d'intervention"
			when "rapport", "facture" then "rapport d'intervention"
			else ""
		end
		
		@appareil = SavAppareil.find(@document.appareil_id)
		partner = ResPartner.find(@appareil.partner)
		partner_facturation = (partner.parent_id.to_i>0 ? ResPartner.find(partner.parent_id) : partner)
		
		bounding_box [bounds.left, bounds.top], :width  => bounds.width do
			unless @partner_society.image.to_s.empty?
				image StringIO.new(Base64.decode64(@partner_society.image)), :height => 100 
			end
		end
		
		formatted_text_box([{ :text => "#{type_document.upcase}\n",
													:size => 	40,
													:font => "Aclonica",
													:color => @red
												},
												{ :text => "#{@document.name}\n",
													:size =>  28,
													:color => "000000"
												}],
												{ :at => [bounds.left, 450],
												  :width => 400,
												  :align => :left }
												)	
												
		start_new_page
	end
	
	def corps
	
		appareil = @appareil
		
		taille_titre = 16
		
		font("Aclonica") do
			text "Etat des lieux", size: taille_titre, color: @red
		end

		data = []
		data << ["Marque", appareil.marque] if appareil.marque
		data << ["Modèle", appareil.modele] if appareil.modele
		type = (appareil.type_id ? SavType.find(appareil.type_id) : nil)
		data << ["Type", type.name] if type
		serial = (appareil.serial ? StockProductionLot.find(appareil.serial) : nil)
		data << ["Numéro de série", serial.name] if serial
		os = (appareil.os_id ? SavOs.find(appareil.os_id) : nil)
		data << ["Système d'exploitation", os.name] if os
		data << ["Observations", @document.observations] if @document.observations
		
		taille_colonnes = [270, 270]
		
		t = table(data) do |table|
			table.cell_style = { :size => 10, :border_color => "000000"}
			table.column(0).style :background_color => @red, :text_color => "ffffff"
			table.column_widths = taille_colonnes
		end
		
		if @document.demande_desc
			move_down 20
			font("Aclonica") do
				text "Description du problème", size: taille_titre, color: @red
			end
			text @document.demande_desc, size: 10
		end
		
		if @document.pannes_constatees
			move_down 20
			font("Aclonica") do
				text "Causes", size: taille_titre, color: @red
			end
			text @document.pannes_constatees, size: 10
		end
		
		if @document.intervention
			move_down 20
			font("Aclonica") do
				text "Résolutions", size: taille_titre, color: @red
			end
			text @document.intervention, size: 10
		end
		
		if @document.preconisation
			move_down 20
			font("Aclonica") do
				text "Préconisations", size: taille_titre, color: @red
			end
			text @document.preconisation, size: 10
		end
		
		res_array = []
		@documents_lignes.each do |ligne|
			produit = ProductProduct.find(ligne.product_id)
			l = []
			l << ligne.name
			l << "%.2f" % ligne.quantity
			l << "%.2f" % ligne.unit_price
			res_array << l
		end
		
		if res_array.count > 0
			move_down 20
			font("Aclonica") do
				text "Matériel et main d'oeuvre", size: taille_titre, color: @red
			end
		
			data = [[]]
			data[0] << "Désignation"
			data[0] << "Qté"
			data[0] << "Prix unitaire"
			data += res_array
		
			taille_colonnes = []
			taille_colonnes << 320
			taille_colonnes << 60
			taille_colonnes << 160
		
			t = table(data) do |table|
				table.row(0).style :background_color => @red, :text_color => "ffffff"
				table.row(0).style :borders => [:top, :bottom, :left, :right]
				table.column(1..2).align = :right
				table.row(0).style :align => :center
				table.row(1..data.count).style :borders => [:left, :right]
				table.cell_style = { :size => 10}
				table.row(0).style :align => :center
				table.column(0).align = :left
				table.column_widths = taille_colonnes
				table.row(data.count-1).style :borders => [:left, :right, :bottom]
			end 
		end
		
		#signature
	end

	def signature
		top = 150
		
		if cursor<100 then
			start_new_page
		end
		
		draw_text "Signature technicien :", :at => [0,top], :size => 8, :styles => [:bold]
		draw_text "Signature client :", :at => [280,top], :size => 8, :styles => [:bold]
		
		top -= 5
		
		stroke_color "aaaaaa"
		rectangle [0,top], 260, 110
		rectangle [280,top], 260, 110
		stroke
	end
	
	def pieds
		texte = "#{@societe.name} - #{@societe.rml_header1} - SIREN: #{@societe.siret}"
		texte += "\nTél: #{@societe.phone} - Email: #{@societe.email} - APE: #{@societe.ape} - RCS/RM: #{@societe.company_registry}"
		number_pages texte, {:start_count_at => 1, :at => [0, 10], :align => :center, :size => 8, :width => 540, :color => @grey_dark}
	end
	
	def rendu fichier
		
		render_file fichier
	
	end
	
end
