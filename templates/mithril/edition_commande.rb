# coding: utf-8

##############################################################################
#		 edition_commande: Report Sale Order for odoo_prawn_report
#
#    Copyright (c) 2014 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re> 
#
# 	 This file is part of odoo_prawn_report.
#
#    odoo_prawn_report is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    odoo_prawn_report is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

class EditionCommande < Edition
	
	def initialize ids, fichier, options
	
		super(ids, fichier, options)
		
		@options = options
			 	 
		i=0
		ids.each { |id|
			preparation id
			en_tete transitaire: nil, 
						partner_id: @document.partner_id, 
						reference: @document.name,
						partner_ref: @document.client_order_ref,
						notes: @document.note,
						date: @document.date_order.strftime("%d/%m/%Y")
			data, taille_colonnes, tva_tab = preparation_lignes
			corps !@document.note.to_s.empty?, data, taille_colonnes
			total tva_tab
			conditions
			i +=1
			start_new_page unless i.eql?(ids.count)
		}
		
		rendu fichier
	end
	
	def preparation id
		@document = SaleOrder.find(id)
		@documents_lignes = SaleOrderLine.where(:order_id => id).order(:sequence,:id)
		
		@type_document = case @document.state
			when "draft", "sent", "cancel" then "devis"
			else "bon de commande"
		end
	
	end
	
	def preparation_lignes
		tva_tab = {}
		taxes = AccountTax.where(:active => true).order(:description)
		taxes.each do |taxe|
			tva_tab.merge!({taxe.id =>{
														:description => taxe.description,
														:name => taxe.name,
														:taux => taxe.amount,
														:total => 0.0,
														:base => 0.0,
								 						}
				 				 		 })
		end
		
		res_array = []
		remise = false
		remise = false
		@documents_lignes.each do |ligne|
			remise = true if ligne.discount>0.0
		end
				
		@documents_lignes.each do |ligne|
			l = []
			l << ligne.name
			orderlinetax = SaleOrderTax.where(:order_line_id => ligne.id).first
			tva = nil
			tva = AccountTax.find(orderlinetax.tax_id) if orderlinetax
			unit = ProductUom.find(ligne.product_uom)
			unit_translate = IrTranslation.where(name: "product.uom,name", src: unit.name).first
			unit_name = (unit_translate ? unit_translate.value : unit.name )
			decimal = (@options['Product Price'] ? @options['Product Price'] : 2)
			l << "%.#{decimal}f €" % ligne.price_unit
			qtite = "%g" % ("%.2f" % ligne.product_uos_qty)
			qtite += " #{unit_name}" if @options[:afficher_unite]
			l <<  qtite 
			l << (ligne.discount>0 ? (ligne.discount.eql?(100.0) ? "Offert !" : "%.2f%" % ligne.discount) : "" ) if remise
			total = ligne.price_unit * ligne.product_uos_qty * (1-ligne.discount/100)
			l << "%.2f €" % total
			l << (tva ? tva.description : "")
			res_array << l
			if tva
				tva_id = tva.id
				tva_tab[tva_id][:total] += (total*tva_tab[tva_id][:taux]).round(2)
				tva_tab[tva_id][:base] += total
			end
		end
		
		data = [[]]
		data[0] << "Désignation"
		data[0] << "P.U. HT"
		data[0] << "Qté"
		data[0] << "Remise %" if remise
		data[0] << "Total HT"
		data[0] << "TVA"
		data += res_array
		
		taille_colonnes = []
		taille_colonnes << (remise ? 280 : 330)
		taille_colonnes << 50
		taille_colonnes << 50
		taille_colonnes << 50 if remise
		taille_colonnes << 60
		taille_colonnes << 50
		
		return data, taille_colonnes, tva_tab
		
	end
	
end
