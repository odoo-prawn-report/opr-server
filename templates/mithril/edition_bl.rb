# coding: utf-8

##############################################################################
#		 edition_commande: Report Sale Order for odoo_prawn_report
#
#    Copyright (c) 2014 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re> 
#
# 	 This file is part of odoo_prawn_report.
#
#    odoo_prawn_report is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    odoo_prawn_report is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

class EditionBl < Edition
	
	def initialize ids, fichier, options
	
		super(ids, fichier, options)
			 	 
		i=0
		ids.each { |id|
			preparation id
			en_tete transitaire: nil, 
						partner_id: @document.partner_id, 
						reference: @document.name,
						origin: @document.origin,
						partner_ref: (@origine ? @origine.client_order_ref : nil),
						notes: @document.note,
						date: @document.date.strftime("%d/%m/%Y"),
						livraison: @livraison
			data, taille_colonnes, qte = preparation_lignes
			corps !@document.note.to_s.empty?, data, taille_colonnes
			quantite_totale qte
			i +=1
			start_new_page unless i.eql?(ids.count)
		}
		
		rendu fichier
	end
	
	def preparation id
		@document = StockPicking.find(id)
		@documents_lignes = StockMove.where(:picking_id => id).order(:id)
		@origine = SaleOrder.where(:name => @document.origin).first
	
		@type_document = case StockPickingType.find(@document.picking_type_id).name
			when "Delivery Orders" then "bon de livraison"
			when "Receipts" then "bon de réception"
			when "PoS Orders" then "commande caisse"
			else "transfert interne"
		end
		
		@livraison = ResPartner.find(@document.partner_id) if @type_document!="bon de réception"
		@livraison = @partner_society if @type_document=="bon de réception"
	end
	
	def preparation_lignes
		
		res_array = []
		quantite_totale = 0.0
		
		@documents_lignes.each do |ligne|
			produit = ProductProduct.find(ligne.product_id)
			l = []
			nom = ligne.name
			serie = []
			operations = StockPackOperation.where(:picking_id => @document.id, :product_id => ligne.product_id)
			operations.each { |op|
				if op.lot_id
					lot = StockProductionLot.find(op.lot_id)
					serie << lot.name
				end
			}
			nom = ligne.name
			nom += "\nN° Série = #{serie.join("-")}" if serie.count>0
			l << nom
			l << "%.2f" % ligne.product_uos_qty
			l << produit.ean13
			quantite_totale += ligne.product_uos_qty
			res_array << l
		end
		
		data = [[]]
		data[0] << "Désignation"
		data[0] << "Qté"
		data[0] << ""
		data += res_array
		
		taille_colonnes = []
		taille_colonnes << 320
		taille_colonnes << 60
		taille_colonnes << 160
		
		return data, taille_colonnes, quantite_totale
		
	end
	
end
