# coding: utf-8

##############################################################################
#		 edition_commande: Report Sale Order for odoo_prawn_report
#
#    Copyright (c) 2014 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re> 
#
# 	 This file is part of odoo_prawn_report.
#
#    odoo_prawn_report is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    odoo_prawn_report is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

class EditionDemandePrixFournisseur < Edition
	
	def initialize ids, fichier, options
	
		super(ids, fichier, options)
			 	 
		i=0
		ids.each { |id|
			preparation id
			en_tete transitaire: nil, 
						partner_id: @document.partner_id, 
						reference: @document.name,
						partner_ref: @document.partner_ref,
						notes: @document.notes,
						date: @document.date_order.strftime("%d/%m/%Y")
			data, taille_colonnes = preparation_lignes
			corps !@document.notes.to_s.empty?, data, taille_colonnes
			i +=1
			start_new_page unless i.eql?(ids.count)
		}
		
		rendu fichier
	end
	
	def preparation id
		@document = PurchaseOrder.find(id)
		@documents_lignes = PurchaseOrderLine.where(:order_id => id).order(:order_id,:id)
		
		devise = ResCurrency.find(@document.currency_id)
		@devise = devise.symbol
		
		@type_document = "Demande de prix"
		
	end
	
	def preparation_lignes
		
		res_array = []
		remise = false
				
		@documents_lignes.each do |ligne|
			l = []
			l << ligne.name
			l << "%i" % ligne.product_qty
			res_array << l
		end
		
		data = [[]]
		data[0] << "Désignation"
		data[0] << "Qté"
		data += res_array
		
		taille_colonnes = []
		taille_colonnes << 400
		taille_colonnes << 140
		
		return data, taille_colonnes
		
	end
	
end
