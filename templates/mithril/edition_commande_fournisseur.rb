# coding: utf-8

##############################################################################
#		 edition_commande: Report Sale Order for odoo_prawn_report
#
#    Copyright (c) 2014 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re> 
#
# 	 This file is part of odoo_prawn_report.
#
#    odoo_prawn_report is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    odoo_prawn_report is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

class EditionCommandeFournisseur < Edition
	
	def initialize ids, fichier, options
	
		super(ids, fichier, options)
		
		@options = options
			 	 
		i=0
		ids.each { |id|
			preparation id
			en_tete transitaire: nil, 
						partner_id: @document.partner_id, 
						reference: @document.name,
						partner_ref: @document.partner_ref,
						notes: @document.notes,
						date: @document.date_order.strftime("%d/%m/%Y")
			data, taille_colonnes, tva_tab = preparation_lignes
			corps !@document.notes.to_s.empty?, data, taille_colonnes
			total tva_tab
			i +=1
			start_new_page unless i.eql?(ids.count)
		}
		
		rendu fichier
	end
	
	def preparation id
		@document = PurchaseOrder.find(id)
		@documents_lignes = PurchaseOrderLine.where(:order_id => id).order(:order_id,:id)
		
		devise = ResCurrency.find(@document.currency_id)
		@devise = devise.symbol
		
		@type_document = "commande fournisseur"
		
	end
	
	def preparation_lignes
		
		tva_tab = {}
		taxes = AccountTax.where(:active => true).order(:description)
		taxes.each do |taxe|
			tva_tab.merge!({taxe.id =>{
														:description => taxe.description,
														:name => taxe.name,
														:taux => taxe.amount,
														:total => 0.0,
														:base => 0.0,
								 						}
				 				 		 })
		end
		
		res_array = []
		remise = false
				
		@documents_lignes.each do |ligne|
			l = []
			l << ligne.name
			orderlinetax = PurchaseOrderTax.where(:ord_id => ligne.id).first
			tva = nil
			tva = AccountTax.find(orderlinetax.tax_id) if orderlinetax
			decimal = (@options['Product Price'] ? @options['Product Price'] : 2)
			l << "%.#{decimal}f #{@devise}" % ligne.price_unit
			l << "%g" % ("%.2f" % ligne.product_qty)
			total = ligne.price_unit * ligne.product_qty
			l << "%.2f #{@devise}" % total
			l << (tva ? tva.description : "")
			res_array << l
			if tva
				tva_id = tva.id
				tva_tab[tva_id][:total] += (total*tva_tab[tva_id][:taux]).round(2)
				tva_tab[tva_id][:base] += total
			end
		end
		
		data = [[]]
		data[0] << "Désignation"
		data[0] << "P.U. HT"
		data[0] << "Qté"
		data[0] << "Total HT"
		data[0] << "TVA"
		data += res_array
		
		taille_colonnes = []
		taille_colonnes << 340
		taille_colonnes << 50
		taille_colonnes << 30
		taille_colonnes << 70
		taille_colonnes << 50
		
		return data, taille_colonnes, tva_tab
		
	end
	
end
