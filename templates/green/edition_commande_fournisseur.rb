# coding: utf-8

##############################################################################
#		 edition_commande: Report Sale Order for odoo_prawn_report
#
#    Copyright (c) 2014 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re> 
#
# 	 This file is part of odoo_prawn_report.
#
#    odoo_prawn_report is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    odoo_prawn_report is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

class EditionCommandeFournisseur < Prawn::Document
	
	def initialize ids, fichier
	
		super(	
					:page_layout => :portrait,
					:right_margin => 15,
					:page_size => 'A4'
			 	 )
			 	 
		@societe = ResCompany.first 
		@partner_society = ResPartner.find(@societe.partner_id)
		
		i=0
		ids.each { |id|
			preparation id
			i +=1
			start_new_page unless i.eql?(ids.count)
		}
		
		rendu fichier
	end
	
	def preparation id
		@document = PurchaseOrder.find(id)
		@documents_lignes = PurchaseOrderLine.where(:order_id => id).order(:order_id,:id)
		
		devise = ResCurrency.find(@document.currency_id)
		@devise = devise.symbol
		
		c = en_tete
		corps c
	end
	
	def en_tete
		
		pas = 10
		text_size = 10
		top = bounds.top-10
		
		top -= pas+35
		
		transitaire = nil
		begin
			transitaire = (@document.transitaire ? ResPartner.find(@document.transitaire) : nil)
		rescue
			transitaire = nil
		end
		
		draw_text "Emetteur :", :at => [0,top], :size => 8, :styles => [:bold]
		draw_text "Transitaire :", :at => [180,top], :size => 8, :styles => [:bold] if transitaire
		draw_text "Adressé à :", :at => [(transitaire ? 360 : 250),top], :size => 8, :styles => [:bold]
		
		top -= pas
		
		fill_color "eeeeee"
		stroke_color "aaaaaa"
		rectangle [0,top+5], (transitaire ? 170 : 230), 120
		fill
		if transitaire
			rectangle [180,top+5], 170, 120
			stroke
		end
		rectangle [(transitaire ? 360 : 250),top+5], (transitaire ? 180 : 290), 120
		stroke
		
		fill_color "000000"
		
		text_size = 10
		
		type_document = "commande fournisseur"
		
		partner = ResPartner.find(@document.partner_id)
		partner_facturation = (partner.parent_id.to_i>0 ? ResPartner.find(partner.parent_id) : partner)
		
		bounding_box [bounds.left, bounds.top+20], :width  => bounds.width do
			unless @partner_society.image.to_s.empty?
				image StringIO.new(Base64.decode64(@partner_society.image)), :height => 70 
			else
				move_down 70
			end
		end
		formatted_text_box([{ :text => "#{type_document.upcase}\n",
						  :size => 	text_size+4,
						  :styles => [:bold]
						},
            { :text => "Réf. : #{@document.name}\n",
						  :size => 	text_size+4,
						  :styles => [:bold]
						},
						{ :text => (@document.partner_ref ? "Réf. fournisseur : #{@document.partner_ref}\n" : ""),
						  :size => 	text_size,
						},
						{ :text => "Date de la commande : #{@document.date_order.strftime("%d/%m/%Y")}\n",
              :size => text_size
            }],
            { :at => [350,bounds.top+10],
              :width => bounds.right-350,
              :align => :right }
            )	
				          	
		formatted_text_box([{ :text => @societe.name+"\n",
							  :size => 	text_size+1,
							  :styles => [:bold]
							},
				            { :text => "#{@partner_society.street}\n#{@partner_society.street2}\n#{@partner_society.zip}\n#{@partner_society.city}\n\n",
				              :size => text_size,
				            },
				            { :text => "Téléphone: #{@partner_society.phone}\nFax: #{@partner_society.fax}\n",
				              :size => text_size,
				            },
				            { :text => "Email: #{@partner_society.email}\n",
				              :size => text_size,
				            },
				            { :text => "Web: #{@partner_society.website}\n",
				              :size => text_size,
				            }],
				            {:at => [7,top],
				            :width => 156}
				            )
		if transitaire
			formatted_text_box([{ :text => "#{transitaire.name}\n\n",
							  :size => 	text_size+1,
							  :styles => [:bold]
							},
				            { :text => "#{transitaire.street}\n#{transitaire.street2}\n",
				              :size => text_size,
				            },
				            { :text => "#{transitaire.zip} #{transitaire.city}\n",
				              :size => text_size,
				            },
				            { :text => (transitaire.phone ? "Tél: #{transitaire.phone}\n" : ""),
				              :size => text_size,
				            },
				            { :text => (transitaire.fax ? "Fax: #{transitaire.fax}\n" : ""),
				              :size => text_size,
				            }],
				            {:at => [187,top],
				            :width => 156}
				            )
		end
		formatted_text_box([{ :text => "#{partner_facturation.name}\n\n",
							  :size => 	text_size+1,
							  :styles => [:bold]
							},
				            { :text => "#{partner_facturation.street}\n#{partner_facturation.street2}\n",
				              :size => text_size,
				            },
				            { :text => "#{partner_facturation.zip} #{partner_facturation.city}\n",
				              :size => text_size,
				            },
				            { :text => (partner.phone ? "Tél: #{partner.phone}\n" : ""),
				              :size => text_size,
				            },
				            { :text => (partner.fax ? "Fax: #{partner.fax}\n" : ""),
				              :size => text_size,
				            },
				            { :text => (partner.email ? "Email: #{partner.email}\n" : ""),
				              :size => text_size,
				            }],
				            {:at => [(transitaire ? 367 : 257),top],
				            :width => 156}
				            )
		top = top-120
		
		unless @document.notes.to_s.empty? then
			bounding_box([0, top], :width => 540) do
				font_size(text_size-4) do
					text @document.notes, :size => text_size
				end
			end
			return true
		else
			return false
		end
	
	end
	
	def corps note=false
		
		tva_tab = {}
		taxes = AccountTax.where(:active => true).order(:description)
		taxes.each do |taxe|
			tva_tab.merge!({taxe.id =>{
														:description => taxe.description,
														:name => taxe.name,
														:taux => taxe.amount,
														:total => 0.0,
														:base => 0.0,
								 						}
				 				 		 })
		end
		
		res_array = []
		remise = false
				
		@documents_lignes.each do |ligne|
			l = []
			l << ligne.name
			orderlinetax = PurchaseOrderTax.where(:ord_id => ligne.id).first
			tva = nil
			tva = AccountTax.find(orderlinetax.tax_id) if orderlinetax
			l << "%.2f #{@devise}" % ligne.price_unit
			l << "%g" % ("%.2f" % ligne.product_qty)
			total = ligne.price_unit * ligne.product_qty
			l << "%.2f #{@devise}" % total
			l << (tva ? tva.description : "")
			res_array << l
			if tva
				tva_id = tva.id
				tva_tab[tva_id][:total] += (total*tva_tab[tva_id][:taux]).round(2)
				tva_tab[tva_id][:base] += total
			end
		end
		
		i = note ? 1 : 10
		text "\n" * i
		data = [[]]
		data[0] << "Désignation"
		data[0] << "P.U. HT"
		data[0] << "Qté"
		data[0] << "Total HT"
		data[0] << "TVA"
		data += res_array
		
		taille_colonnes = []
		taille_colonnes << 340
		taille_colonnes << 50
		taille_colonnes << 30
		taille_colonnes << 70
		taille_colonnes << 50
		
		t = table(data) do |table|
			table.header = true
			table.row(0).style :borders => [:top, :bottom, :left, :right]
			table.row(0).style :align => :center
			table.row(1..data.count).style :borders => [:left, :right]
			table.cell_style = { :size => 8, :border_color => "aaaaaa"}
			table.column(1..5).align = :right
			table.row(0).style :align => :center
			table.column(0).align = :left
			table.column_widths = taille_colonnes
		end 
		
		top = cursor
		
		left = bounds.left
		bottom = 200
		if top<bottom then
			bottom = 14
		end
		line [left,top], [left,bottom]
		taille_colonnes.each { |i|
			left += i
			line [left,top], [left,bottom]
		}
		line [bounds.left,bottom], [left,bottom]
		stroke
  		
		if bottom<200 then
			start_new_page
		end
		
		pieds @documents_lignes, tva_tab
	
	end
	
	def pieds lignes, tva_tab

		top = 200
		text_size = 8
		
		data = []
		
		data << ["Total HT", "%.2f #{@devise}" % @document.amount_untaxed]
		#data << ["Total taxes", "%.2f #{@devise}" % @document.amount_tax]
		
		tva_tab.each do |key, value|
			data << [value[:name], "%.2f #{@devise}" % value[:total]] if value[:total] > 0
		end
		
		data << ["Total TTC", "%.2f #{@devise}" % @document.amount_total]
		
		bounding_box([310,top], :width => 300, :height => 200) do
			
			table(data, :cell_style => { :borders => [], :padding => 1, :size => text_size+2 }) do |table|
	  			table.column(0).align = :left
	  			table.column(1).align = :right
	  			table.row(data.count).style :background_color => "eeeeee"
	  			table.row(data.count-1).style :background_color => "eeeeee"
	  			table.column(0).width = 150
	  			table.column(1).width = 80
			end 
		end
		
	end
	
	def rendu fichier
	
		number_pages "#{@societe.rml_header1} - SIREN: #{@societe.siret}", {:start_count_at => 1, :at => [0, 10], :align => :center, :size => 8}
		number_pages "APE: #{@societe.ape} - RCS/RM: #{@societe.company_registry}", {:start_count_at => 1, :at => [0, 0], :align => :center, :size => 8}
		#number_pages "<page>/<total>", {:start_count_at => 1, :at => [bounds.right - 100, 0], :align => :right, :size => 8}
		render_file fichier
	
	end
	
end
