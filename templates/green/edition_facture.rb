# coding: utf-8

##############################################################################
#		 edition_facture: Report Sale Invoice for odoo_prawn_report
#
#    Copyright (c) 2014 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re> 
#
# 	 This file is part of odoo_prawn_report.
#
#    odoo_prawn_report is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    odoo_prawn_report is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

class EditionFacture < Prawn::Document
	
	def initialize ids, fichier, options
	
		super(	
					:page_layout => :portrait,
					:right_margin => 15,
					:page_size => 'A4'
			 	 )
		
		@societe = ResCompany.first 
		@partner_society = ResPartner.find(@societe.partner_id)
		
		@options = options
		
		i=0
		ids.each { |id|
			preparation id
			i +=1
			start_new_page unless i.eql?(ids.count)
		}
		
		rendu fichier
	end
	
	def preparation id
		@document = AccountInvoice.find(id)
		@documents_lignes = AccountInvoiceLine.where(:invoice_id => id).order(:sequence,:id)
		@journal = AccountJournal.find(@document.journal_id)
	
		c = en_tete
		corps c
	end
	
	def en_tete
		
		pas = 10
		text_size = 10
		top = bounds.top-20
		
		top -= pas+35
		
		if @journal.type == 'purchase'
			draw_text "Adressé à :", :at => [0,top], :size => 8, :styles => [:bold]
			draw_text "Emetteur :", :at => [250,top], :size => 8, :styles => [:bold]
		else
			draw_text "Emetteur :", :at => [0,top], :size => 8, :styles => [:bold]
			draw_text "Adressé à :", :at => [250,top], :size => 8, :styles => [:bold]
		end
		
		top -= pas
		
		fill_color "eeeeee"
		stroke_color "aaaaaa"
		rectangle [0,top+5], 230, 110
		fill
		rectangle [250,top+5], 290, 110
		stroke
		
		fill_color "000000"
		
		text_size = 10
		
		user = (@document.user_id ? ResUser.find(@document.user_id) : nil)
		vendeur = (user ? ResPartner.find(user.partner_id) : nil)
		
		bounding_box [bounds.left, bounds.top+20], :width  => bounds.width do
			unless @partner_society.image.to_s.empty?
				image StringIO.new(Base64.decode64(@partner_society.image)), :height => 70 
			else
				move_down 70
			end
		end
		
		type = 'Facture'
		type = 'Facture' if @document.state == "draft"
		type = 'Pro-Forma' if @document.state == "proforma2"
		type = 'Avoir' if @journal.type == 'sale_refund'
		type = 'Facture fournisseur' if @journal.type == 'purchase'
		
		
		formatted_text_box([{ :text => "#{type.upcase}\n",
						  :size => 	text_size+4,
						  :styles => [:bold]
						},
            { :text => (@document.number.to_s.empty? ? "Réf. : #{@document.reference.to_s}\n" : "Réf. : #{@document.number.to_s}\n"),
						  :size => 	text_size+4,
						  :styles => [:bold]
						},
						{ :text => ((@document.reference and !@document.number.to_s.empty?) ? "Document d'origine : #{@document.reference}\n" : ""),
              :size => text_size
            },
						{ :text => (@document.date_invoice ? "Date de la facture : #{@document.date_invoice.strftime("%d/%m/%Y")}\n" : ""),
              :size => text_size
            },
            { :text => (@document.date_due ? "Date d'échéance : #{@document.date_due.strftime("%d/%m/%Y")}\n" : ""),
              :size => text_size
            },
            { :text => (vendeur ? "Affaire suivie par #{vendeur.name}" : ""),
              :size => text_size
            }],
            { :at => [345,bounds.top+10],
              :width => bounds.right-350,
              :align => :right }
            )	
		
		partner = ResPartner.find(@document.partner_id)
		partner_facturation = (partner.parent_id.to_i>0 ? ResPartner.find(partner.parent_id) : partner)
						          	
		formatted_text_box([{ :text => @societe.name+"\n",
							  :size => 	text_size+1,
							  :styles => [:bold]
							},
				            { :text => "#{@partner_society.street}\n#{@partner_society.street2}\n#{@partner_society.zip}\n#{@partner_society.city}\n\n",
				              :size => text_size,
				            },
				            { :text => "Téléphone: #{@partner_society.phone} - Fax: #{@partner_society.fax}\n",
				              :size => text_size,
				            },
				            { :text => "Email: #{@partner_society.email}\n",
				              :size => text_size,
				            },
				            { :text => "Web: #{@partner_society.website}\n",
				              :size => text_size,
				            }],
				            {:at => [7,top]}
				            )
		formatted_text_box([{ :text => "#{partner_facturation.name}\n\n",
							  :size => 	text_size+1,
							  :styles => [:bold]
							},
				            { :text => "#{partner_facturation.street}\n#{partner_facturation.street2}\n",
				              :size => text_size,
				            },
				            { :text => "#{partner_facturation.zip} #{partner_facturation.city}",
				              :size => text_size,
				            }],
				            {:at => [257,top]}
				            )
		top = top-120
		
		unless @document.comment.to_s.empty? then
			bounding_box([0, top], :width => 540) do
				font_size(text_size-4) do
					text @document.comment, :size => text_size
				end
				#stroke_bounds
			end
			return true
		else
			return false
		end
	
	end
	
	def corps note=false
		
		tva_tab = {}
		taxes = AccountTax.where(:active => true).order(:description)
		taxes.each do |taxe|
			tva_tab.merge!({taxe.id =>{
														:description => taxe.description,
														:name => taxe.name,
														:taux => taxe.amount,
														:total => 0.0,
														:base => 0.0,
								 						}
				 				 		 })
		end
		
		res_array = []
		remise = false
		@documents_lignes.each do |ligne|
			remise = true if ligne.discount>0.0
		end
				
		@documents_lignes.each do |ligne|
			l = []
			l << ligne.name
			orderlinetax = AccountInvoiceLineTax.where(:invoice_line_id => ligne.id).first
			tva = nil
			tva = AccountTax.find(orderlinetax.tax_id) if orderlinetax
			unit = ProductUom.find(ligne.uos_id)
			unit_translate = IrTranslation.where(name: "product.uom,name", src: unit.name).first
			unit_name = (unit_translate ? unit_translate.value : unit.name )
			l << "%.2f €" % ligne.price_unit
			qtite = "%g" % ("%.2f" % ligne.quantity)
			qtite += " #{unit_name}" if @options[:afficher_unite]
			l <<  qtite 
			remise_ligne = (ligne.discount.to_f>0.0 ? (ligne.discount.eql?(100.0) ? "Offert !" : "%.2f%" % ligne.discount.to_f) : "" )
			l << remise_ligne if remise
			total = ligne.price_unit * ligne.quantity * (1-ligne.discount/100)
			l << "%.2f €" % total
			l << (tva ? tva.description : "")
			res_array << l
			if tva
				tva_id = tva.id
				tva_tab[tva_id][:total] += (total*tva_tab[tva_id][:taux]).round(2)
				tva_tab[tva_id][:base] += total
			end
		end
		
		i = note ? 1 : 10
		text "\n" * i
		data = [[]]
		#data[0] << "Code"
		data[0] << "Désignation"
		data[0] << "P.U. HT"
		data[0] << "Qté"
		data[0] << "Remise %" if remise
		data[0] << "Total HT"
		data[0] << "TVA"
		data += res_array

		taille_colonnes = []
		taille_colonnes << (remise ? 280 : 330)
		taille_colonnes << 50
		taille_colonnes << 50
		taille_colonnes << 50 if remise
		taille_colonnes << 60
		taille_colonnes << 50
		
		t = table(data) do |table|
			table.header = true
			table.row(0).style :borders => [:top, :bottom, :left, :right]
			table.row(0).style :align => :center
			table.row(1..data.count).style :borders => [:left, :right]
			#table.row(data.count-1).style :borders => [:left, :right, :bottom]
			table.cell_style = { :size => 8, :border_color => "aaaaaa"}
			table.column(1..5).align = :right
			table.row(0).style :align => :center
			table.column(0).align = :left
			table.column_widths = taille_colonnes
		end 
		
		top = cursor
		
		left = bounds.left
		bottom = 200
		if top<bottom then
			bottom = 14
		end
		line [left,top], [left,bottom]
		taille_colonnes.each { |i|
			left += i
			line [left,top], [left,bottom]
		}
		line [bounds.left,bottom], [left,bottom]
		stroke
  		
		if bottom<200 then
			start_new_page
		end
		
		pieds @documents_lignes, tva_tab
	
	end
	
	def pieds lignes, tva_tab

		loi = %{
			Suivant la loi n°92-1442 du 31/12/92, seront appliqués:\n- Escompte 0% pour paiement anticipé.\n- Pénalités de 1.5 fois l'intérêt légal par mois pour paiement en retard.\n\nConformément aux dispositions de la loi du 12/05/1980, le transfert de propriété de la marchandise vendue est suspendu jusqu'au paiement intégral du prix.
		}
		
		top = 200
		text_size = 8
		
		data = []
		
		data << ["Total HT", "%.2f €" % @document.amount_untaxed]
		#data << ["Total taxes", "%.2f €" % @document.amount_tax]
		
		tva_tab.each do |key, value|
			data << [value[:name], "%.2f €" % value[:total]] if value[:total] > 0
		end
		
		data << ["Total TTC", "%.2f €" % @document.amount_total]
		
		if (@document.residual<@document.amount_total and !@document.number.to_s.empty?	and @journal.type != 'sale_refund')
			data << ["Payé", "%.2f €" % (@document.amount_total-@document.residual)]
			data << ["Reste à payer", "%.2f €" % @document.residual]
		end
		
		bounding_box([310,top], :width => 300, :height => 200) do
			
			table(data, :cell_style => { :borders => [], :padding => 1, :size => text_size+2 }) do |table|
	  			table.column(0).align = :left
	  			table.column(1).align = :right
	  			table.row(data.count).style :background_color => "eeeeee"
	  			table.row(data.count-1).style :background_color => "eeeeee"
	  			table.column(0).width = 150
	  			table.column(1).width = 80
			end 
		end
		
		# Liste des paiements déjà effectués
		moveline = AccountMoveLine.where("move_id = #{@document.move_id} and reconcile_id > 0").first if @document.move_id
		if moveline	
			paiements = AccountMoveLine.where(:reconcile_id => moveline.reconcile_id)
			if paiements.count>1
				data = [["Date", "Mode", "Référence", "Montant"]]
				paiements.each { |p|
					if p.move_id!=@document.move_id
						journal = AccountJournal.find(p.journal_id)
						if @journal.type == 'purchase'
							data << [p.date.strftime("%d/%m/%Y"), journal.name, p.ref, "%.2f €" % p.debit]
						else
							data << [p.date.strftime("%d/%m/%Y"), journal.name, p.ref, "%.2f €" % p.credit]
						end
					end
				}

				bounding_box([310,top-90], :width => 300) do	
					text "Règlements déjà effectués :", :size => text_size, :style => :bold
					table(data, :cell_style => { :borders => [], :padding => 1, :size => text_size-2 }) do |table|
						table.row(0).font_style = :bold
						table.column(0).width = 50
						table.column(1).width = 50
						table.column(2).width = 60
						table.column(3).width = 60
					end
				end
			end
		end
		# Condition et mode de règlement
		compte = ResPartnerBank.where(:partner_id => @partner_society.id).first
		banque = ResBank.find(compte.bank)
		condition = ""
		if @document.payment_term
			payment_term = AccountPaymentTerm.find(@document.payment_term)
			condition = payment_term.note 
		end
		formatted_text_box([{ :text => "Condition de règlement: #{condition}\n\n",
							  :size => 	text_size,
							  :styles => [:bold]
							},
				            { :text => "Règlement par chèque à l'ordre de #{@societe.name}\n",
				              :size => text_size,
				              :styles => [:bold]
				            },
				            { :text => "envoyé au:\n",
				              :size => text_size,
				              :styles => [:bold]
				            },
				            { :text => "#{@partner_society.street}\n#{@partner_society.street2}\n#{@partner_society.zip} #{@partner_society.city}\n\n",
				              :size => text_size
				            },
				            { :text => "Règlement par virement sur le compte bancaire suivant:\n",
				              :size => text_size,
				              :styles => [:bold]
				            },
				            { :text => (compte ? "Banque: #{banque.name}\n" : ""),
				              :size => text_size
				            },
				            { :text => (compte ? "BIC: #{compte.bank_bic}\n" : ""),
				              :size => text_size
				            },
				            { :text => (compte ? "IBAN: #{compte.acc_number}\n" : ""),
				              :size => text_size
				            },
				            { :text => loi,
				              :size => text_size-2
				            }],
				            {:at => [0,top-5]}
				            )
		
	end
	
	def rendu fichier
	
		number_pages "#{@societe.rml_header1} - SIREN: #{@societe.siret}", {:start_count_at => 1, :at => [0, 10], :align => :center, :size => 8}
		number_pages "APE: #{@societe.ape} - RCS/RM: #{@societe.company_registry}", {:start_count_at => 1, :at => [0, 0], :align => :center, :size => 8}
		#number_pages "<page>/<total>", {:start_count_at => 1, :at => [bounds.right - 100, 0], :align => :right, :size => 8}
		render_file fichier
	
	end
	
end
