# coding: utf-8

##############################################################################
#		 edition_commande: Report Sale Order for odoo_prawn_report
#
#    Copyright (c) 2014 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re> 
#
# 	 This file is part of odoo_prawn_report.
#
#    odoo_prawn_report is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    odoo_prawn_report is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

class EditionBl < Prawn::Document
	
	def initialize ids, fichier
	
		super(	
					:page_layout => :portrait,
					:right_margin => 15,
					:page_size => 'A4'
			 	 )
			 	 
		@societe = ResCompany.first 
		@partner_society = ResPartner.find(@societe.partner_id)
		
		i=0
		ids.each { |id|
			preparation id
			i +=1
			start_new_page unless i.eql?(ids.count)
		}
		
		rendu fichier
	end
	
	def preparation id
		@document = StockPicking.find(id)
		@documents_lignes = StockMove.where(:picking_id => id).order(:id)
		@origine = SaleOrder.where(:name => @document.origin).first
	
		c = en_tete
		corps c
	end
	
	def en_tete
		
		pas = 10
		text_size = 10
		top = bounds.top-10
		
		top -= pas+50
		
		draw_text "Emetteur :", :at => [0,top], :size => 8, :styles => [:bold]
		draw_text "Adresse de livraison :", :at => [180,top], :size => 8, :styles => [:bold]
		draw_text "Adresse de facturation :", :at => [360,top], :size => 8, :styles => [:bold]
		
		top -= pas
		
		fill_color "eeeeee"
		stroke_color "aaaaaa"
		rectangle [0,top+5], 170, 120
		fill
		rectangle [180,top+5], 170, 120
		stroke
		rectangle [360,top+5], 180, 120
		stroke
		
		fill_color "000000"
		
		text_size = 10
		
		type_document = case StockPickingType.find(@document.picking_type_id).name
			when "Delivery Orders" then "bon de livraison"
			when "Receipts" then "bon de réception"
			when "PoS Orders" then "commande caisse"
			else "transfert interne"
		end
		
		partner = ResPartner.find(@document.partner_id)
		partner_facturation = (partner.parent_id.to_i>0 ? ResPartner.find(partner.parent_id) : partner)
		
		bounding_box [bounds.left, bounds.top+20], :width  => bounds.width do
			unless @partner_society.image.to_s.empty?
				image StringIO.new(Base64.decode64(@partner_society.image)), :height => 70 
			else
				move_down 70
			end
		end
		formatted_text_box([{ :text => "#{type_document.upcase}\n",
													:size => 	text_size+4,
													:styles => [:bold]
												},
												{ :text => "Réf. : #{@document.name}\n",
													:size => 	text_size+4,
													:styles => [:bold]
												},
												{ :text => "Date du #{type_document} : #{@document.date.strftime("%d/%m/%Y")}\n",
												  :size => text_size
												},
												{ :text => (@document.origin ? "Document d'origine : #{@document.origin}\n" : ""),
												  :size => text_size
												},
												{ :text => (@origine ? (@origine.client_order_ref ? "Document d'origine client : #{@origine.client_order_ref}" : "") : ""),
												  :size => text_size
												}],
												{ :at => [350,bounds.top+10],
												  :width => bounds.right-350,
												  :align => :right }
												)	
				          	
		formatted_text_box([{ :text => @societe.name+"\n",
							  :size => 	text_size+1,
							  :styles => [:bold]
							},
				            { :text => "#{@partner_society.street}\n#{@partner_society.street2}\n#{@partner_society.zip}\n#{@partner_society.city}\n\n",
				              :size => text_size,
				            },
				            { :text => "Téléphone: #{@partner_society.phone}\nFax: #{@partner_society.fax}\n",
				              :size => text_size,
				            },
				            { :text => "Email: #{@partner_society.email}\n",
				              :size => text_size,
				            },
				            { :text => "Web: #{@partner_society.website}\n",
				              :size => text_size,
				            }],
				            {:at => [7,top],
				            :width => 156}
				            )
		formatted_text_box([{ :text => "#{partner.name}\n\n",
							  :size => 	text_size+1,
							  :styles => [:bold]
							},
				            { :text => "#{partner.street}\n#{partner.street2}\n",
				              :size => text_size,
				            },
				            { :text => "#{partner.zip} #{partner.city}\n\n",
				              :size => text_size,
				            },
				            { :text => (partner.phone ? "Tél: #{partner.phone}" : ""),
				              :size => text_size,
				            }],
				            {:at => [187,top],
				            :width => 156}
				            )

		formatted_text_box([{ :text => "#{partner_facturation.name}\n\n",
							  :size => 	text_size+1,
							  :styles => [:bold]
							},
				            { :text => "#{partner_facturation.street}\n#{partner_facturation.street2}\n",
				              :size => text_size,
				            },
				            { :text => "#{partner_facturation.zip} #{partner_facturation.city}\n\n",
				              :size => text_size,
				            },
				            { :text => (partner_facturation.phone ? "Tél: #{partner_facturation.phone}" : ""),
				              :size => text_size,
				            }],
				            {:at => [367,top],
				            :width => 156}
				            )
		top = top-120
		
		unless @document.note.to_s.empty? then
			bounding_box([0, top], :width => 540) do
				font_size(text_size-4) do
					text @document.note, :size => text_size
				end
			end
			return true
		else
			return false
		end
	
	end
	
	def corps note=false
		
		res_array = []
		quantite_totale = 0.0
		
		@documents_lignes.each do |ligne|
			produit = ProductProduct.find(ligne.product_id)
			l = []
			nom = ligne.name
			serie = []
			operations = StockPackOperation.where(:picking_id => @document.id, :product_id => ligne.product_id)
			operations.each { |op|
				if op.lot_id
					lot = StockProductionLot.find(op.lot_id)
					serie << lot.name
				end
			}
			nom = ligne.name
			nom += "\nN° Série = #{serie.join("-")}" if serie.count>0
			l << nom
			l << "%.2f" % ligne.product_uos_qty
			l << produit.ean13
			quantite_totale += ligne.product_uos_qty
			res_array << l
		end
		
		i = note ? 1 : 11
		text "\n" * i
		data = [[]]
		#data[0] << "Code"
		data[0] << "Désignation"
		data[0] << "Qté"
		data[0] << ""
		data += res_array
		
		taille_colonnes = []
		taille_colonnes << 320
		taille_colonnes << 60
		taille_colonnes << 160
		
		t = table(data) do |table|
			table.header = true
			table.row(0).style :borders => [:top, :bottom, :left, :right]
			table.row(0).style :align => :center
			table.row(1..data.count).style :borders => [:left, :right]
			#table.row(data.count-1).style :borders => [:left, :right, :bottom]
			table.cell_style = { :size => 8, :border_color => "aaaaaa"}
			table.row(0).style :align => :center
			table.column(0).align = :left
			table.column(2).align = :center
			table.column_widths = taille_colonnes
			table.column(2).style :font => @@template_folder + "/fonts/3OF9_NEW.TTF", :size => 16
		end 
		
		top = cursor
		
		left = bounds.left
		bottom = 80
		if top<bottom then
			bottom = 14
		end
		line [left,top], [left,bottom]
		taille_colonnes.each { |i|
			left += i
			line [left,top], [left,bottom]
		}
		line [bounds.left,bottom], [left,bottom]
		stroke
  		
		if bottom<20 then
			start_new_page
		end
		
		pieds quantite_totale
	
	end
	
	def pieds quantite_totale

		top = 70
		text_size = 8
		
		data = []
		
		data << ["Quantité totale", "%.2f" % quantite_totale]
		
		bounding_box([390,top], :width => 150, :height => 200) do
			
			table(data, :cell_style => { :borders => [], :padding => 1, :size => text_size+2 }) do |table|
	  			table.column(0).align = :left
	  			table.column(1).align = :right
	  			table.row(data.count).style :background_color => "eeeeee"
	  			table.row(data.count-1).style :background_color => "eeeeee"
	  			table.column(0).width = 100
	  			table.column(1).width = 50
			end 
		end
		
	end
	
	def rendu fichier
	
		number_pages "#{@societe.rml_header1} - SIREN: #{@societe.siret}", {:start_count_at => 1, :at => [0, 10], :align => :center, :size => 8}
		number_pages "APE: #{@societe.ape} - RCS/RM: #{@societe.company_registry}", {:start_count_at => 1, :at => [0, 0], :align => :center, :size => 8}
		render_file fichier
	
	end
	
end
