# coding: utf-8

##############################################################################
#		 edition_commande: Report Sale Order for odoo_prawn_report
#
#    Copyright (c) 2014 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re> 
#
# 	 This file is part of odoo_prawn_report.
#
#    odoo_prawn_report is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    odoo_prawn_report is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

class EditionBi < Prawn::Document
	
	def initialize ids, fichier, options
	
		super(	
					:page_layout => :portrait,
					:right_margin => 15,
					:page_size => 'A4',
					:background => @@template_folder+"/images/color.jpg"
			 	 )
			 	 
		@societe = ResCompany.first 
		@partner_society = ResPartner.find(@societe.partner_id)
		
		i=0
		ids.each { |id|
			preparation id
			i +=1
			start_new_page unless i.eql?(ids.count)
		}
		
		rendu fichier
	end
	
	def preparation id
		@document = SavIntervention.find(id)
		@documents_lignes = SavInterventionLine.where(:intervention_id => id).order(:id)
		@text_size = 10
		@gris = "fefefe"
		@jaune = "ffe182"
		@noir = "000000"
		@vert = "cdffcc"
		
		en_tete
		infos
		descriptif
		if ["demande", "bon"].include?(@document.state)
		  prise_en_charge
		  status_demande
		else
		  feuille_temps
		  rapport
		  articles
		end
		observations
	end
	
	def en_tete
		
		top = bounds.top
				
		type_document = case @document.state
			when "demande" then "demande d'intervention"
			when "bon" then "bon d'intervention"
			when "rapport", "facture" then "rapport d'intervention"
			else ""
		end
		
		cadre @jaune, 10, bounds.top-39, bounds.right-10, 10
		
		bounding_box [bounds.left, bounds.top+20], :width  => bounds.width do
			unless @partner_society.image.to_s.empty?
				image StringIO.new(Base64.decode64(@partner_society.image)), :height => 70 
			else
				move_down 70
			end
		end
		
		top = bounds.top
		bounding_box([80, top], :width => 340) do
		  text "<b>#{type_document.upcase}</b>", :size => @text_size+6, :inline_format => true
		  text "<b>N° #{@document.name}</b>", :size => @text_size+6, :inline_format => true
		end
		draw_text "DATE: #{@document.date_demande.strftime('%d/%m/%Y')}", :at => [450,bounds.top-10], :size => @text_size
		
	end
	
	def infos
	  top = bounds.top-70
	  bounding_box([0, top], :width => 240) do
	    text "<b>INFORMATIONS EQUIPEMENT</b>", :size => @text_size, :inline_format => true
	  end
	  left = 300
	  bounding_box([left, top], :width => 240) do
	    text "<b>INFORMATIONS CONCERNANT L'INTERVENTION</b>", :size => @text_size, :inline_format => true
	  end
	  
	  data = []
	  appareil = (@document.appareil_id ? SavAppareil.find(@document.appareil_id : nil)
	  partner = (@document.partner ? ResPartner.find(@document.partner) : nil)
	  serial = (appreil ? StockProductionLot.find(appareil.serial) : nil)
	  product = (serial ? ProductProduct.find(serial.product_id) : nil)
		data << ["Client", (partner ? partner.name : "")]
		data << ["Equipement", (appareil ? appareil.name : "")]
		data << ["Article", (product ? product.name_template : "")]
		data << ["Numéro de série", serial ? serial.name: ""]
		data << ["Demandeur", @document.demandeur]
		data << ["N° DI client", @document.numero_interne]
		data << ["Date de fin de garantie", (appareil ? (appareil.garantie_date_fin ? appareil.garantie_date_fin.strftime('%d/%m/%Y') : "") : "")]
		data << ["Date de mise en service", (appareil ? (appareil.date_service ? appareil.date_service.strftime('%d/%m/%Y') : "") : "")]
		
		top = cursor-5
		width = left-40
		cadre @jaune, 0, top+8, width, 2
		bounding_box [bounds.left, top], :width  => width do
		  t = table(data, :cell_style => {:borders => [], :size => 7, :padding => 0}) do |table|
			  table.row(0..data.count-1).align = :left
			  table.column(1).style(:borders => [:bottom], :border_color => @gris)
			  table.column_widths = [width/2-30, width/2+30]
			  table.cells.style(:padding_top => 3, :padding_bottom => 1)
		  end
		end
		
		data = []
    symptomes = SavSymptome.find(@document.symptome_id) if @document.symptome_id
    technicien = SavTechnicien.find(@document.technicien) if @document.technicien
    contrat = AccountAnalyticAccount.find(appareil.contract_id) if (appareil and appareil.contract_id)
    
		data << ["Symptômes", (symptomes ? symptomes.name : "")]
		data << ["Type d'intervention", @document.type_intervention.to_s.upcase]
		data << ["Facturation", @document.facturable_mo]
		data << ["N° contrat", (contrat ? contrat.name : "")]
		data << ["Technicien", (technicien ? technicien.name : "")]
		data << ["Date d'intervention prévue", (@document.date_intervention ? @document.date_intervention.strftime('%d/%m/%Y') : "")]
		data << ["Heure prévue", (@document.date_intervention ? (@document.date_intervention+4.hours).strftime('%Hh%M') : "")]
		
		width = bounds.right-left
		cadre @jaune, left, top+8, width, 2
		bounding_box [left, top], :width  => width do
		  t = table(data, :cell_style => {:borders => [], :size => 7, :padding => 0}) do |table|
			  table.row(0..data.count-1).align = :left
			  table.column(1).style(:borders => [:bottom], :border_color => @gris)
			  table.column_widths = [width/2-30, width/2+30]
			  table.cells.style(:padding_top => 3, :padding_bottom => 1)
		  end
		end
	end
	
	def descriptif
	  move_down 30	
		text "<b>DESCRIPTIF DE LA DEMANDE</b>", :size => @text_size, :inline_format => true
		
		cadre @jaune, 0, cursor+2, bounds.right, 2
		height = 50
		cadre @gris, 0, cursor, bounds.right, height		
		bounding_box [5, cursor-5], :width => bounds.right-10, :height => height do
		  text @document.demande_desc, :size => 7
		end
	end
	
	def prise_en_charge
	  move_down 5	
		text "<b>PRISE EN CHARGE</b>", :size => @text_size, :inline_format => true
		cadre @jaune, 0, cursor+2, bounds.right, 2
		
		etapes = [{:nom => "Prise en charge client",
		           :date => @document.date_prise_en_charge
		          },
		          {:nom => "Entrée atelier",
		           :date => @document.date_entree_atelier
		          },
		          {:nom => "Décontamination",
		           :date => @document.date_decontamination
		          },
		          {:nom => "Diagnostique",
		           :date => @document.date_diagnostique
		          },
		          {:nom => "Demande de devis",
		           :date => @document.date_demande_devis
		          },
		         ]
		
		move_down 5
		etapes.each do |etape|
		  top = cursor
		  text etape[:nom], :size => 7
		  coche 150, top, !etape[:date].nil?
		  p etape[:date]-@document.date_demande if etape[:date]
		  draw_text (etape[:date] ? "#{etape[:date].strftime('%d/%m/%Y')} => #{(etape[:date]-@document.date_demande).to_i}j" : ""), :size => 7, :at => [300, top-5]
		end
	end
	
	def feuille_temps
	  move_down 5
		text "<b>FEUILLE DE TEMPS</b>", :size => @text_size, :inline_format => true
		cadre @jaune, 0, cursor+2, bounds.right, 2
		
		data = []
		data << ["Technicien", "Date", "Heure de début", "Heure de fin"]
		techs = SavInterventionTechnicien.where(:intervention_id => @document.id).order(:id)
		techs.each do |tech|
		  technicien = SavTechnicien.find(tech.technicien_id)
		  data << [technicien.name, tech.date_debut.strftime("%d/%m/%Y"), (tech.date_debut+4.hour).strftime("%H:%M"), (tech.date_fin+4.hour).strftime("%H:%M")]
		end
		
		t = table(data, :cell_style => {:borders => [:left, :right, :top, :bottom], :size => 7, :padding => 2}) do |table|
		  table.header = true
		  table.row(0).font_style = :bold
		  table.cells.align = :center
		  table.cells.background_color = @gris
		  table.column_widths = [136, 136, 136, 136]
		  table.row(0).size = 9
		  table.row(0).borders = [:left, :right, :bottom]
	  end
	end
	
	def rapport
    move_down 15
		text "<b>RAPPORT D'ACTIVITES</b>", :size => @text_size, :inline_format => true
		cadre @jaune, 0, cursor+2, bounds.right, 2
		
		data = []
    symptomes = SavSymptome.find(@document.symptome_id) if @document.symptome_id
    outil = SavOutil.find(@document.outil_id) if @document.outil_id
    
		data << ["Panne déclarée:", (symptomes ? symptomes.name : "")]
		data << ["Panne constatée:", @document.pannes_constatees]
		data << ["Tests réalisés:", @document.tests]
		data << ["Commentaires:", @document.intervention]
		data << ["Utilisations outils de contrôle:", (outil ? outil.name : "")]
		data << ["Utilisations check-list:", ""]
		
		move_down 3
		t = table(data, :cell_style => {:borders => [], :size => 7, :padding => 2}) do |table|
		  table.header = true
		  table.column(0).font_style = :bold
		  table.column(0).align = :right
		  table.column(1).align = :left
		  table.column(1).row(3).background_color = @gris
		  table.column(1).row(3).borders = [:left, :right, :top, :bottom]
		  table.column_widths = [150, 394]
	  end
	end
	
	def articles
    move_down 15
		text "<b>ARTICLES</b>", :size => @text_size, :inline_format => true
		cadre @jaune, 0, cursor+2, bounds.right, 2
		
		total = 0.0
		res_array = []
		@documents_lignes.each do |ligne|
			produit = ProductProduct.find(ligne.product_id)
			l = []
			l << ligne.name
			l << "%.2f" % ligne.quantity
			l << "%.2f €" % ligne.unit_price
			total += ligne.unit_price.round(2)
			res_array << l
		end
		
		res_array << ["", "TOTAL HT", "#{total.round(2)} €"]
		
		data = [[]]
		data[0] << "Désignation"
		data[0] << "Qté"
		data[0] << "Prix HT"
		data += res_array
		
		t = table(data, :cell_style => {:size => 7, :padding => 2}) do |table|
			table.header = true
			table.cells.background_color = @gris
			table.row(0).size = 9
			table.column(2).align = :right
			table.row(0).align = :center
			table.columns(0..1).align = :center
			table.row(0).font_style = :bold
			table.column_widths = [320, 60, 164]
			table.row(data.count-1).column(0).style(:borders => [], :background_color => @vert, :size => 9)
			table.row(data.count-1).font_style = :bold
			table.row(0).borders = [:left, :right, :bottom]
		end 
	end
	
	def status_demande
	  move_down 15	
		text "<b>STATUT DE LA DEMANDE</b>", :size => @text_size, :inline_format => true
		
		cadre @jaune, 0, cursor+2, bounds.right, 2
		height = 50
		cadre @gris, 0, cursor, bounds.right, height	
		bounding_box [5, cursor-5], :width => bounds.right-10, :height => height do
		  text "", :size => 7
		end	
	end
	
	def observations
	  move_down 15	
		text "<b>OBSERVATIONS</b>", :size => @text_size, :inline_format => true
		
		cadre @jaune, 0, cursor+2, bounds.right, 2
		height = 50
		cadre @gris, 0, cursor, bounds.right, height		
		bounding_box [5, cursor-5], :width => bounds.right-10, :height => height do
		  text @document.observations, :size => 7
		end
		left = 450
		bounding_box [left, cursor-15], :width => bounds.right-left do
		  text "<b>Signature :</b>", :size => 10, :inline_format => true
		end
	end
	
	def cadre couleur, x, y, w, h
	  fill_color couleur
		rectangle [x,y], w, h
		fill
		fill_color @noir
	end
	
	def coche x, y, selected=true
	  stroke_color @noir
		rectangle [x,y], 5, 5
		stroke
		if selected 
		  line [x,y], [x+5,y-5]
		  line [x,y-5], [x+5,y]
		  stroke
		end
	end
	
	def rendu fichier
	
		number_pages "#{@societe.name} - #{@societe.rml_header1} - SIREN: #{@societe.siret}", {:start_count_at => 1, :at => [0, 8], :align => :center, :size => 6}
		number_pages "APE: #{@societe.ape} - RCS/RM: #{@societe.company_registry}", {:start_count_at => 1, :at => [0, 0], :align => :center, :size => 6}
		render_file fichier
	
	end
	
end
