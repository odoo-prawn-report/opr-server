# coding: utf-8

##############################################################################
#		 edition_commande: Report Sale Order for odoo_prawn_report
#
#    Copyright (c) 2014 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re> 
#
# 	 This file is part of odoo_prawn_report.
#
#    odoo_prawn_report is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    odoo_prawn_report is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

class EditionBi < Prawn::Document
	
	def initialize ids, fichier, options
	
		super(	
					:page_layout => :portrait,
					:right_margin => 15,
					:page_size => 'A4'
			 	 )
			 	 
		@societe = ResCompany.first 
		@partner_society = ResPartner.find(@societe.partner_id)
		
		
		i=0
		ids.each { |id|
			preparation id
			i +=1
			start_new_page unless i.eql?(ids.count)
		}
		
		rendu fichier
	end
	
	def preparation id
		@document = SavIntervention.find(id)
		@documents_lignes = SavInterventionLine.where(:intervention_id => id).order(:id)
	
		en_tete
		corps
	end
	
	def en_tete
		
		pas = 10
		text_size = 10
		top = bounds.top-10
		
		top -= pas+35
		
		draw_text "Lieu de l'intervention :", :at => [180,top], :size => 8, :styles => [:bold]
		draw_text "Client :", :at => [360,top], :size => 8, :styles => [:bold]
		
		top -= pas
		
		fill_color "eeeeee"
		stroke_color "aaaaaa"
		rectangle [0,top+5], 170, 120
		fill
		rectangle [180,top+5], 170, 120
		stroke
		rectangle [360,top+5], 180, 120
		stroke
		
		fill_color "000000"
		
		text_size = 10
		
		type_document = case @document.state
			when "demande" then "demande d'intervention"
			when "bon" then "bon d'intervention"
			when "rapport", "facture" then "rapport d'intervention"
			else ""
		end
		
		partner = ResPartner.find(@document.partner)
		partner_facturation = (partner.parent_id.to_i>0 ? ResPartner.find(partner.parent_id) : partner)
		
		bounding_box [bounds.left, bounds.top+20], :width  => bounds.width do
			unless @partner_society.image.to_s.empty?
				image StringIO.new(Base64.decode64(@partner_society.image)), :height => 70 
			else
				move_down 70
			end
		end
		formatted_text_box(
						[{ :text => "#{type_document.upcase}\n",
							:size => 	text_size+4,
							:styles => [:bold]
							},
		          { :text => "Réf. : #{@document.name}\n",
								:size => 	text_size+4,
								:styles => [:bold]
							},
							{ :text => "Num. demande du client : #{@document.numero_interne}\n",
								:size => 	text_size
							},
							{ :text => "Date de la demande : #{(@document.date_demande ? @document.date_demande.strftime("%d/%m/%Y") : "")}\n",
		            :size => text_size
		          }],
            { :at => [300,bounds.top+10],
              :width => bounds.right-300,
              :align => :right }
            )	
				          	
		formatted_text_box([{ :text => @societe.name+"\n",
							  :size => 	text_size+1,
							  :styles => [:bold]
							},
				            { :text => "#{@partner_society.street}\n#{@partner_society.street2}#{@partner_society.street2 ? "\n" : ""}#{@partner_society.zip}\n#{@partner_society.city}\n\n",
				              :size => text_size,
				            },
				            { :text => "Téléphone: #{@partner_society.phone}\nFax: #{@partner_society.fax}\n",
				              :size => text_size,
				            },
				            { :text => "Email: #{@partner_society.email}\n",
				              :size => text_size,
				            },
				            { :text => "Web: #{@partner_society.website}\n",
				              :size => text_size,
				            }],
				            {:at => [7,top],
				            :width => 156}
				            )
		formatted_text_box([{ :text => "#{partner.name}\n\n",
							  :size => 	text_size+1,
							  :styles => [:bold]
							},
				            { :text => "#{partner.street}\n#{partner.street2}#{partner.street2 ? "\n" : ""}",
				              :size => text_size,
				            },
				            { :text => "#{partner.zip} #{partner.city}\n\n",
				              :size => text_size,
				            },
				            { :text => "Tél: #{partner.phone}",
				              :size => text_size,
				            }],
				            {:at => [187,top],
				            :width => 156}
				            )

		formatted_text_box([{ :text => "#{partner_facturation.name}\n\n",
							  :size => 	text_size+1,
							  :styles => [:bold]
							},
				            { :text => "#{partner_facturation.street}\n#{partner_facturation.street2}#{partner_facturation.street2 ? "\n" : ""}",
				              :size => text_size,
				            },
				            { :text => "#{partner_facturation.zip} #{partner_facturation.city}\n\n",
				              :size => text_size,
				            },
				            { :text => "Tél: #{partner_facturation.phone}\nFax: #{partner_facturation.fax}",
				              :size => text_size,
				            }],
				            {:at => [367,top],
				            :width => 156}
				            )
	end
	
	def corps
	
		appareil = SavAppareil.find(@document.appareil_id)
	
		text "\n" * 10
	
		data = [[],[]]
		data[0] << "Appareil concerné - Numéro de série"
		data[0] << "Numéro interne"
		data[0] << "Date de mise en service"
		data[1] << appareil.name
		data[1] << appareil.serie_interne.to_s
		data[1] << (appareil.date_service ? appareil.date_service.strftime('%d/%m/%Y') : "")
		
		taille_colonnes = []
		taille_colonnes << 320
		taille_colonnes << 110
		taille_colonnes << 110
		
		t = table(data) do |table|
			table.header = true
			table.row(0..1).style :borders => [:top, :bottom, :left, :right]
			table.column(2).align = :right
			table.row(0).style :align => :center
			table.cell_style = { :size => 8, :border_color => "aaaaaa"}
			table.row(0).style :align => :center
			table.column(0).align = :left
			table.column_widths = taille_colonnes
		end 
		
		text_size = 10
		draw_text "Panne constatée :", :at => [0,cursor-20], :size => 10, :styles => [:bold]
		bounding_box([0, cursor-30], :width => 540) do
			font_size(text_size-4) do
				text @document.pannes, :size => text_size
			end
		end
		
		tech = (@document.technicien ? SavTechnicien.find(@document.technicien) : nil)
		
		top = cursor-7
		
		stroke_color "aaaaaa"
		rectangle [0,top], 540, 20
		stroke
		
		top -= 13
		
		draw_text "Technicien : #{(tech ? tech.name.to_s : "_ _ _ _ _ _ _ _ _ _")}", :at => [7,top], :size => 10
		draw_text "Date d'intervention : #{(@document.date_intervention ? @document.date_intervention.strftime('%d/%m/%Y') : "_ _ _ _ _ _ _ _ _ _")}", :at => [150,top], :size => 10
		draw_text "Numero du rapport manuel : #{@document.numero_rapport_manuel || "_ _ _ _ _ _ _ _"}", :at => [340,top], :size => 10
		
		text "\n" * 4
		draw_text "Description de l'intervention :", :at => [0,cursor+10], :size => 10, :styles => [:bold]
		bounding_box([0, cursor], :width => 540) do
			font_size(text_size-4) do
				text @document.intervention, :size => text_size
			end
		end
		
		text "\n" * 4 if @document.intervention.to_s.empty?
		
		res_array = []
		@documents_lignes.each do |ligne|
			produit = ProductProduct.find(ligne.product_id)
			l = []
			l << ligne.name
			l << "%.2f" % ligne.quantity
			l << "%.2f" % ligne.unit_price
			res_array << l
		end
		
		draw_text "Matériel et main d'oeuvre utilisés :", :at => [0,cursor-20], :size => 10, :styles => [:bold]
		
		text "\n" * 2
		
		data = [[]]
		data[0] << "Désignation"
		data[0] << "Qté"
		data[0] << "Prix unitaire"
		data += res_array
		
		taille_colonnes = []
		taille_colonnes << 320
		taille_colonnes << 60
		taille_colonnes << 160
		
		t = table(data) do |table|
			table.header = true
			table.row(0).style :borders => [:top, :bottom, :left, :right]
			table.column(2).align = :right
			table.row(0).style :align => :center
			table.row(1..data.count).style :borders => [:left, :right]
			table.cell_style = { :size => 8, :border_color => "aaaaaa"}
			table.row(0).style :align => :center
			table.column(0).align = :left
			table.column_widths = taille_colonnes
		end 
		
		left = bounds.left
		bottom = 200
		top = cursor
		if top<bottom then
			bottom = 14
		end
		line [left,top], [left,bottom]
		taille_colonnes.each { |i|
			left += i
			line [left,top], [left,bottom]
		}
		line [bounds.left,bottom], [left,bottom]
		stroke
  		
		if bottom<100 then
			start_new_page
		end
		
		top = bottom - 20 
		
		draw_text "Signature technicien :", :at => [0,top], :size => 8, :styles => [:bold]
		draw_text "Signature client :", :at => [280,top], :size => 8, :styles => [:bold]
		
		top -= 5
		
		stroke_color "aaaaaa"
		rectangle [0,top], 260, 110
		rectangle [280,top], 260, 110
		stroke
		
		#pieds
	
	end
	
	def pieds

		top = 190
		text_size = 8
		
		data = []
		
		data << ["Quantité totale", "%.2f" % quantite_totale]
		
		bounding_box([390,top], :width => 150, :height => 200) do
			
			table(data, :cell_style => { :borders => [], :padding => 1, :size => text_size+2 }) do |table|
	  			table.column(0).align = :left
	  			table.column(1).align = :right
	  			table.row(data.count).style :background_color => "eeeeee"
	  			table.row(data.count-1).style :background_color => "eeeeee"
	  			table.column(0).width = 100
	  			table.column(1).width = 50
			end 
		end
		
	end
	
	def rendu fichier
	
		number_pages "#{@societe.rml_header1} - SIRET: #{@societe.siret}", {:start_count_at => 1, :at => [0, 10], :align => :center, :size => 8}
		number_pages "APE: #{@societe.ape} - RCS/RM: #{@societe.company_registry}", {:start_count_at => 1, :at => [0, 0], :align => :center, :size => 8}
		render_file fichier
	
	end
	
end
