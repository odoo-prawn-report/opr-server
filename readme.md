# Odoo Prawn Report #

OPR Server est un serveur d'édition pour Odoo.
Il fonctionne à base d'un serveur Ruby/Sinatra qui renvoie des editions PDF faites avec la bibliothèque Prawn.

## Installation

* Ajouter le module OPR-Module aux modules de Odoo.
* Récupérer le serveur odoo-prawn-report
* Créer dans le dossier **config** un fichier de configuration s'appelant **db.yaml** et ressemblant à ceci :

        adapter: postgresql
        host: 127.0.0.1
        database: databasename
        port: '5432'
        username: odoo
        password: password

* Vous devez ensuite installer ruby, bundle et lancer bundle install pour installer les gems nécessaires.

## Lancement

* Le serveur doit pouvoir être démarré de cette façon :

>ruby odoo_prawn_report.rb

* Si tout fonctionne bien, vous pouvez configurer le serveur pour qu'il fonctionne avec apache ou nginx (pensez à modifier l'url dans le module Odoo OPR-module)

## Modifications

Les éditions se trouvent dans le dossier templates. 

Pour le moment voici les éditions présentes :

* édition de devis/commande
* édition de facture (client, fournisseur) et avoir
* édition de livraison/réception
* édition des bon d'intervention du module odoo_sav
* édition des commandes d'achats et demandes de prix

Les objets activerecord accessibles sont décrits dans le fichier models.rb

## Sécurisation

Le serveur odoo-prawn-report doit être hébergé par un serveur nginx ou apache et doit être lancé sur un port uniquement accessible à Odoo en utilisant par exemple un pare-feu.

Exemple de configuration d'un NGINX sur le port 5006 :

     server {
       listen 5006;
       server_name mon.serveur.com;
       root /opt/odoo/odoo-prawn-report/public;
       passenger_enabled on;
     }

